import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './features/home/home.component';
import { LoginComponent } from './features/login/login.component';
import { DriverComponent } from './features/driver/driver.component';
import { FuelComponent } from './features/fuel/fuel.component';
import { FuelStationComponent } from './features/fuel-station/fuel-station.component';
import {  HistoryComponent} from './features/history/history.component';
import { MaintenanceComponent } from './features/maintenance/maintenance.component';
import { TruckComponent } from './features/truck/truck.component';
import { MaintenanceDetailComponent } from './features/maintenance-detail/maintenance-detail.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'driver', component: DriverComponent},
  { path: 'fuel', component: FuelComponent},
  { path: 'fuel-station', component: FuelStationComponent},
  { path: 'history', component: HistoryComponent},
  { path: 'maintenance', component: MaintenanceComponent},
  { path: 'truck', component: TruckComponent},
  { path: 'maintenance-detail', component: MaintenanceDetailComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
