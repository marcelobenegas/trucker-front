import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/core/data/data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: any;

  constructor(
    private router: Router,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.user = this.dataService.getCurrentUser();
  }
  goToHome() {
    this.router.navigateByUrl('/home');
  }
  goToLogin() {
    this.router.navigateByUrl('/login');
  }
  goToDrivers(){
    this.router.navigateByUrl('/driver');
  }
  goToFuel(){
    this.router.navigateByUrl('/fuel');
  }

}
