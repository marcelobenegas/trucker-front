import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertMaintenanceComponent } from './modal-insert-maintenance.component';

describe('ModalInsertMaintenanceComponent', () => {
  let component: ModalInsertMaintenanceComponent;
  let fixture: ComponentFixture<ModalInsertMaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertMaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
