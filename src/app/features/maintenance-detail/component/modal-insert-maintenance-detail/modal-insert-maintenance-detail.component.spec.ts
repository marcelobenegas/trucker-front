import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertMaintenanceDetailComponent } from './modal-insert-maintenance-detail.component';

describe('ModalInsertMaintenanceDetailComponent', () => {
  let component: ModalInsertMaintenanceDetailComponent;
  let fixture: ComponentFixture<ModalInsertMaintenanceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertMaintenanceDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertMaintenanceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
