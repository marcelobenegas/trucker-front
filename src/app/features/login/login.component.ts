import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input() user_name : string;
  @Input () password : string;
  public submitted: boolean = false;
  loading = false;
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      user_name: ['', Validators.required,],
      password: ['', Validators.required,]
    });
  }
  get f() { return this.loginForm.controls; }

  // getAllUser() {
  //   console.log("OK")
  //   this.authService.getAllUser().subscribe((response) => {
  //     console.log(response);
  //   })
  // }
  goToHome() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }
    if (this.loginForm.valid) {
      this.authService.auth(this.loginForm.value.userName, this.loginForm.value.password).subscribe((response)=> {
        });
    }
  }
}
