import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FuelStationService } from 'src/app/core/services/fuel_station/fuel-station.service';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-insert-fuel-station',
  templateUrl: './modal-insert-fuel-station.component.html',
  styleUrls: ['./modal-insert-fuel-station.component.scss']
})
export class ModalInsertFuelStationComponent implements OnInit {

  @Input() id: number;
  @Input() liter_price: number;
  @Input() name: string;
  @Input() location: string;
  @Output() closeModals = new EventEmitter<string>();
  @Output() refreshtable = new EventEmitter<any>();
  public textSpinner: string;
  public submitted: boolean = false;
  public selectCountry = '';
  public selectStatus;
  public update: boolean = false;
  public title: string;
  public createForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private fuelStationService: FuelStationService,
    private spinner: NgxSpinnerService 
  ) { }

  ngOnInit() {
    if (this.id != undefined) {
      this.update = true
      this.title = 'Actualizar ' + this.id;
    } 
    else {
      this.title = 'Ingresar Estacion';
    }
    this.createForms()
  }
  createForms() {
    this.createForm = this.formBuilder.group({
//      id: ['', Validators.required],
liter_price: ['', Validators.required],
      name: ['', Validators.required],
      location: ['', Validators.required]
    });
    if (this.id != undefined) {
      //      this.createForm.controls['id'].setValue(this.id);
            this.createForm.controls['liter_price'].setValue(this.liter_price);
            this.createForm.controls['name'].setValue(this.name);
            this.createForm.controls['location'].setValue(this.location);
    }
  }
  get f() { return this.createForm.controls; }
  createStation() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Guardando...'
      this.spinner.show();
      this.fuelStationService.createStation(this.createForm.value.id, this.createForm.value.liter_price,
        this.createForm.value.name, this.createForm.value.location).subscribe((data) => {
          this.refreshtable.emit();
          this.closeModal();
          Swal.fire('Se ha ingresado la estacion correctamente', 'Ingreso correcto');
        });
    }
  }
  updateStation() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Actualizando...'
      this.spinner.show();
      this.fuelStationService.updateStation(this.id, this.createForm.value.liter_price,
        this.createForm.value.name,this.createForm.value.location).subscribe((data) => {
        this.refreshtable.emit();
        this.closeModal();
        Swal.fire('Se ha actualizado la estacion correctamente', 'Actualizacion correcta');
      });
    }
  }
  closeModal() {
    this.closeModals.emit();
  }
  
}
