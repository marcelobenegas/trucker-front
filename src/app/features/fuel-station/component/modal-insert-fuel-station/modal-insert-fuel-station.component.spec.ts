import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertFuelStationComponent } from './modal-insert-fuel-station.component';

describe('ModalInsertFuelStationComponent', () => {
  let component: ModalInsertFuelStationComponent;
  let fixture: ComponentFixture<ModalInsertFuelStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertFuelStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertFuelStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
