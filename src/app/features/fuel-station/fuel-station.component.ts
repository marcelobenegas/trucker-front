import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { FuelStationService } from 'src/app/core/services/fuel_station/fuel-station.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { Fuel_Station } from 'src/app/core/model/fuel_station';
import { NgbdSortableHeader, SortEvent } from 'src/app/core/directives/directive';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-fuel-station',
  templateUrl: './fuel-station.component.html',
  styleUrls: ['./fuel-station.component.scss']
})
export class FuelStationComponent implements OnInit {

  public datos: any = [];
  public id: number;
  public liter_price: number;
  public name: string;
  public location: string;
  public textSpinner:string;
  fuel_station$: Observable<Fuel_Station[]>;
  total$: Observable<number>;
  public closeResult: any;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;


  constructor(private fuelStationService : FuelStationService,
    private modalService : NgbModal,
    private spinner: NgxSpinnerService) 
    
    {
      this.fuel_station$ = this.fuelStationService.countries$;
      this.total$ = this.fuelStationService.total$;
    }

  ngOnInit() {
    console.log('oks')
    this.getStations();
  }

  getStations(){
    this.fuelStationService.getStation().subscribe((data)=>{
      console.log(data)
    })
  }

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }else{
        return null;
      }
    });

    this.fuelStationService.sortColumn = column;
    this.fuelStationService.sortDirection = direction;
  }

  openModal(content) {
    this.id = undefined;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalEdit(content, id, liter_price, name, location) {
    this.id = id;
    this.liter_price = liter_price;
    this.name = name;
    this.location = location;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }


  openModalDelete(content, id) {
    this.id = id;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

   delete() {
     this.textSpinner = 'Eliminando...'
       this.spinner.show();
       this.fuelStationService.deleteStation(this.id).subscribe((data) => {   
         console.log(data)
         //Settimeout sacarlo despues es solo para efectos de ejemplo
         setTimeout(() => {
           console.log(data);
           this.closeModal();
           this.refreshtable();
           this.spinner.hide();
           Swal.fire('Se ha eliminado la estacion correctamente', 'Eliminación correcta' ,
             //positionClass: 'toast-bottom-right'
          );
         })
       });
   }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  refreshtable(){
    this.getStations();
    this.spinner.hide();
  }

  closeModal() {
    this.modalService.dismissAll();
  }

}
