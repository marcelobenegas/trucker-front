import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-fuel',
  templateUrl: './fuel.component.html',
  styleUrls: ['./fuel.component.scss']
})
export class FuelComponent implements OnInit {
  public textSpinner:string;
  public closeResult: any;

  constructor(
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService
  ) { }

  ngOnInit() {
  }

}
