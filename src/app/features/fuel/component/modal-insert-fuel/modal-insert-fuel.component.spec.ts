import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertFuelComponent } from './modal-insert-fuel.component';

describe('ModalInsertFuelComponent', () => {
  let component: ModalInsertFuelComponent;
  let fixture: ComponentFixture<ModalInsertFuelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertFuelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertFuelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
