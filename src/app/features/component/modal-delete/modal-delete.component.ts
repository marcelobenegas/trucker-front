import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss']
})
export class ModalDeleteComponent implements OnInit {

  @Output() closeModals = new EventEmitter<string>();
  @Output() deleteData = new EventEmitter<any>();
  @Input() name: string;
  @Input() id: number;
  public title: string;
  constructor(
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.title = '¿Seguro deseas eliminar?';
  }
  delete() {
    this.deleteData.emit();
  }

  refreshtable(){
    this.spinner.hide();
  }

  closeModal() {
    this.modalService.dismissAll();
  }
}
