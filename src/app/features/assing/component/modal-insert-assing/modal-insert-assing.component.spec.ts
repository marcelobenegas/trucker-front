import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertAssingComponent } from './modal-insert-assing.component';

describe('ModalInsertAssingComponent', () => {
  let component: ModalInsertAssingComponent;
  let fixture: ComponentFixture<ModalInsertAssingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertAssingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertAssingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
