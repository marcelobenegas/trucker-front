import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import {DriverService} from '../../core/services/driver/driver.service';
import { DecimalPipe } from '@angular/common';
import { Driver } from 'src/app/core/model/driver';
import { Observable } from 'rxjs';
import { NgbdSortableHeader, SortEvent } from 'src/app/core/directives/directive';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss'],
  providers: [DriverService, DecimalPipe]
})
export class DriverComponent implements OnInit {
  public datos: any = [];
  public id: number;
  public run: string;
  public name: string;
  public last_name: string;
  public textSpinner:string;
  driver$: Observable<Driver[]>;
  total$: Observable<number>;
  public closeResult: any;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
  constructor(private driverService : DriverService,   
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService) {   
     this.driver$ = this.driverService.countries$;
      this.total$ = this.driverService.total$; }

  ngOnInit() {
    this.getDrivers();
  }

  getDrivers(){
    this.driverService.getDrivers().subscribe((data)=>{
      console.log(data)
    })
  }

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.driverService.sortColumn = column;
    this.driverService.sortDirection = direction;
  }

  openModal(content) {
    this.id = undefined;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  openModalEdit(content, id, run, name, last_name) {
    this.id = id;
    this.run = run;
    this.name = name;
    this.last_name = last_name;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalDelete(content, id) {
    this.id = id;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  delete() {
    this.textSpinner = 'Eliminando...'
      this.spinner.show();
      this.driverService.deleteDriver(this.id).subscribe((data) => {   
        console.log(data)
        //Settimeout sacarlo despues es solo para efectos de ejemplo
        setTimeout(() => {
          console.log(data);
          this.closeModal();
          this.refreshtable();
          this.spinner.hide();
          this.toastr.success('Se ha eliminado el conductor correctamente', 'Eliminación correcta' ,{
            positionClass: 'toast-bottom-right'
          });
        })
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  refreshtable(){
    this.getDrivers();
    this.spinner.hide();
  }

  closeModal() {
    this.modalService.dismissAll();
  }

}
