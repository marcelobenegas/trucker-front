import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DriverService } from 'src/app/core/services/driver/driver.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-modal-insert-driver',
  templateUrl: './modal-insert-driver.component.html',
  styleUrls: ['./modal-insert-driver.component.scss']
})
export class ModelInsertDriverComponent implements OnInit {
  @Input() id: number;
  @Input() run: string;
  @Input() name: string;
  @Input() last_name: string;
  @Output() closeModals = new EventEmitter<string>();
  @Output() refreshtable = new EventEmitter<any>();
  public textSpinner: string;
  public submitted: boolean = false;
  public selectCountry = '';
  public selectStatus;
  public update: boolean = false;
  public title: string;
  public createForm: FormGroup;
  constructor(    private formBuilder: FormBuilder,
    private driverService: DriverService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService) { }

  ngOnInit() {
    if (this.id != undefined) {
      this.update = true
      this.title = 'Actualizar ' + this.name;
    } 
    else {
      this.title = 'Ingresar Conductor';
    }
    this.createForms()

  }

  createForms() {
    this.createForm = this.formBuilder.group({
      // id: ['', Validators.required],
      run: ['', Validators.required],
      name: ['', Validators.required],
      last_name: ['', Validators.required]
    });
    if (this.id != undefined) {
      // this.createForm.controls['id'].setValue(this.id);
      this.createForm.controls['run'].setValue(this.run);
      this.createForm.controls['name'].setValue(this.name);
      this.createForm.controls['last_name'].setValue(this.last_name);
    }
  }

  get d() { return this.createForm.controls; }

  createDriver() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Guardando...'
      this.spinner.show();
      this.driverService.createDriver(this.createForm.value.id,this.createForm.value.run,this.createForm.value.name,this.createForm.value.last_name).subscribe((data) => {
          this.RefreshTable();
          this.closeModal();
          this.toastr.success('Se ha ingresado el conductor correctamente', 'Ingreso correcto', {
            positionClass: 'toast-bottom-right'
          });
        });
    }
  }

  updateDriver() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Actualizando...'
      this.spinner.show();
      this.driverService.updateDriver(this.id, this.createForm.value.run , this.createForm.value.name, this.createForm.value.last_name).subscribe((data) => {
        this.RefreshTable();
        this.closeModal();
        this.toastr.success('Se ha actualizado el conductor correctamente', 'Actualizacion correcta', {
          positionClass: 'toast-bottom-right'
        });
      });
    }
  }

  closeModal() {
    this.closeModals.emit();
  }
  RefreshTable(){
    this.refreshtable.emit();
  }
}
