import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelInsertDriverComponent } from './modal-insert-driver.component';

describe('ModelInsertDriverComponent', () => {
  let component: ModelInsertDriverComponent;
  let fixture: ComponentFixture<ModelInsertDriverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelInsertDriverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelInsertDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
