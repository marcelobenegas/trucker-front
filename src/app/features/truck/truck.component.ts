import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { TruckService } from 'src/app/core/services/truck/truck.service';
import { Observable } from 'rxjs';
import { NgbdSortableHeader, SortEvent } from 'src/app/core/directives/directive';
import { Truck } from 'src/app/core/model/truck';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-truck',
  templateUrl: './truck.component.html',
  styleUrls: ['./truck.component.scss'],
  providers: [TruckService, DecimalPipe]
})
export class TruckComponent implements OnInit {
  public datos: any = [];
  public id: number;
  public plate: string;
  public brand: string;
  public model: string;
  public textSpinner:string;
  truck$: Observable<Truck[]>;
  total$: Observable<number>;
  public closeResult: any;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;


  constructor(private truckService : TruckService,
    private modalService : NgbModal,
    private spinner: NgxSpinnerService)
     {
      this.truck$ = this.truckService.countries$;
      this.total$ = this.truckService.total$;
     }

  ngOnInit() {
    console.log('oks')
    this.getTrucks();
  }
  getTrucks(){
    this.truckService.getTruck().subscribe((data)=>{
      console.log(data)
    })
  }
  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }else{
        return null;
      }
    });

    this.truckService.sortColumn = column;
    this.truckService.sortDirection = direction;
  }

  openModal(content) {
    this.id = undefined;
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  openModalEdit(content, id, plate, brand, model) {
    this.id = id;
    this.plate = plate;
    this.brand = brand;
    this.model = model;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }


  openModalDelete(content, id) {
    this.id = id;
  
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true, windowClass:"xlModal modal-holder" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  delete() {
    this.textSpinner = 'Eliminando...'
      this.spinner.show();
      this.truckService.deleteTruck(this.id).subscribe((data) => {   
        console.log(data)
        //Settimeout sacarlo despues es solo para efectos de ejemplo
        setTimeout(() => {
          console.log(data);
          this.closeModal();
          this.refreshtable();
          this.spinner.hide();
          Swal.fire('Se ha eliminado el camion correctamente', 'Eliminación correcta' ,
            //positionClass: 'toast-bottom-right'
         );
        })
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  refreshtable(){
    this.getTrucks();
    this.spinner.hide();
  }

  closeModal() {
    this.modalService.dismissAll();
  }

}
