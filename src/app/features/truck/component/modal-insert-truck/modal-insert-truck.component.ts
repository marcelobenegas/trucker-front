import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { TruckService } from 'src/app/core/services/truck/truck.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-modal-insert-truck',
  templateUrl: './modal-insert-truck.component.html',
  styleUrls: ['./modal-insert-truck.component.scss']
})
export class ModalInsertTruckComponent implements OnInit {

  
  @Input() id: number;
  @Input() plate: string;
  @Input() brand: string;
  @Input() model: string;
  @Output() closeModals = new EventEmitter<string>();
  @Output() refreshtable = new EventEmitter<any>();
  public textSpinner: string;
  public submitted: boolean = false;
  public selectCountry = '';
  public selectStatus;
  public update: boolean = false;
  public title: string;
  public createForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private truckService: TruckService,
    private spinner: NgxSpinnerService 
  ) { }

  ngOnInit() {
    if (this.id != undefined && this.plate != undefined) {
      this.update = true
      this.title = 'Actualizar ' + this.id;
    } 
    else {
      this.title = 'Ingresar Camion';
    }
    this.createForms()
  }
  createForms() {
    this.createForm = this.formBuilder.group({
//      id: ['', Validators.required],
      plate: ['', Validators.required],
      brand: ['', Validators.required],
      model: ['', Validators.required]
    });
    if (this.id != undefined) {
//      this.createForm.controls['id'].setValue(this.id);
      this.createForm.controls['plate'].setValue(this.plate);
      this.createForm.controls['brand'].setValue(this.brand);
      this.createForm.controls['model'].setValue(this.model);
    }
  }
  get t() { return this.createForm.controls; }
  createTruck() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Guardando...'
      this.spinner.show();
      this.truckService.createTruck(this.createForm.value.id, this.createForm.value.plate,
        this.createForm.value.brand, this.createForm.value.model).subscribe((data) => {
          this.refreshtable.emit();
          this.closeModal();
          Swal.fire('Se ha ingresado el Camion correctamente', 'Ingreso correcto');
        });
    }
  }
  updateTruck() {
    this.submitted = true;
    if (this.createForm.invalid) {
      return;
    }
    if (this.createForm.valid) {
      this.textSpinner = 'Actualizando...'
      this.spinner.show();
      this.truckService.updateTruck(this.id, this.createForm.value.plate,
        this.createForm.value.brand,this.createForm.value.model).subscribe((data) => {
        this.refreshtable.emit();
        this.closeModal();
        Swal.fire('Se ha actualizado el camion correctamente', 'Actualizacion correcta');
      });
    }
  }
  closeModal() {
    this.closeModals.emit();
  }
  

}
