import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertTruckComponent } from './modal-insert-truck.component';

describe('ModalInsertTruckComponent', () => {
  let component: ModalInsertTruckComponent;
  let fixture: ComponentFixture<ModalInsertTruckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertTruckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertTruckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
