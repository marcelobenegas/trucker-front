import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertHistoryComponent } from './modal-insert-history.component';

describe('ModalInsertHistoryComponent', () => {
  let component: ModalInsertHistoryComponent;
  let fixture: ComponentFixture<ModalInsertHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
