import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './features/home/home.component';
import { LoginComponent } from './features/login/login.component';
import { HeaderComponent } from './features/header/header.component';
import { DriverComponent } from './features/driver/driver.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModelInsertDriverComponent } from './features/driver/components/modal-insert-driver/modal-insert-driver.component';
// import { ModalDeleteDriverComponent } from './features/driver/components/modal-delete-driver/modal-delete-driver/modal-delete-driver.component';
import { ModalDeleteComponent } from './features/component/modal-delete/modal-delete.component';
import { FuelStationComponent } from './features/fuel-station/fuel-station.component';
import { FuelComponent } from './features/fuel/fuel.component';
import { HistoryComponent } from './features/history/history.component';
import { MaintenanceComponent } from './features/maintenance/maintenance.component';
import { TruckComponent } from './features/truck/truck.component';
import { ModalInsertTruckComponent } from './features/truck/component/modal-insert-truck/modal-insert-truck.component';
import { ModalInsertMaintenanceComponent } from './features/maintenance/component/modal-insert-maintenance/modal-insert-maintenance.component';
import { ModalInsertHistoryComponent } from './features/history/component/modal-insert-history/modal-insert-history.component';
import { ModalInsertFuelStationComponent } from './features/fuel-station/component/modal-insert-fuel-station/modal-insert-fuel-station.component';
import { ModalInsertFuelComponent } from './features/fuel/component/modal-insert-fuel/modal-insert-fuel.component';
import { MaintenanceDetailComponent } from './features/maintenance-detail/maintenance-detail.component';
import { ModalInsertMaintenanceDetailComponent } from './features/maintenance-detail/component/modal-insert-maintenance-detail/modal-insert-maintenance-detail.component';
import { AssingComponent } from './features/assing/assing.component';
import { ModalInsertAssingComponent } from './features/assing/component/modal-insert-assing/modal-insert-assing.component';
import {A11yModule} from '@angular/cdk/a11y';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { NgbdSortableHeader } from './core/directives/directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent,
    DriverComponent,
    DashboardComponent,
    ModelInsertDriverComponent,
    // ModalDeleteDriverComponent,
    ModalDeleteComponent,
    FuelStationComponent,
    FuelComponent,
    HistoryComponent,
    MaintenanceComponent,
    TruckComponent,
    ModalInsertTruckComponent,
    ModalInsertMaintenanceComponent,
    ModalInsertHistoryComponent,
    ModalInsertFuelStationComponent,
    ModalInsertFuelComponent,
    MaintenanceDetailComponent,
    ModalInsertMaintenanceDetailComponent,
    AssingComponent,
    ModalInsertAssingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
