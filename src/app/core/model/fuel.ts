export interface Fuel{
    id: number;
    liter_quantity: number;
    date: Date;
    id_driver: number;
    name: string;
    id_truck: number;
    plate: string;
    id_fuel_s: number;
    name_station: string;
}