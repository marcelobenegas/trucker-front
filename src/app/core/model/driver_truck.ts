export interface Driver_Truck{
    id: number;
    id_driver: number;
    name: string;
    last_name: string;
    id_truck: number;
    plate: string;
    date_start: string;
}