export interface Fuel_Station{
    id: number;
    liter_price: number;
    name: string;
    location: string;
}