export interface Truck{
    id: number;
    plate: string;
    model: string;
    brand: string;
    id_driver: number;
    name: string;
}