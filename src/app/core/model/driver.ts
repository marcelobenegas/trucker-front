export interface Driver{
    id: number;
    run: string;
    name: string;
    lastname: string;
}