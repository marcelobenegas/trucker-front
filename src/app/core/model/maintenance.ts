export interface Maintenance{
    id: number;
    maintance_date: Date;
    description: string;
    place: string;
    id_driver: number;
    name: string;
    last_name: string;
    id_truck: number;
    plate: string;
}