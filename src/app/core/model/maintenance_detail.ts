export interface Maintance_Detail{
    id: number;
    detail: string;
    unitary_price: number;
    total_price:number;
    id_maintenance: number;
}