export interface History{
    id: number;
    id_truck: number;
    plate: string;
    id_driver: number;
    name: string;
    last_name: string;
    date: Date;
}