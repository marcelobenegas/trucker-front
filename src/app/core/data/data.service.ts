import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  getCurrentUser(): any {
    return {
      id: 1,
      name: 'Administrador',
      mail: 'mimail@miamil.com',
      username: '@yeti',
      charge: 'Software Developer',
      // tslint:disable-next-line: max-line-length
      avatar: 'https://disenopaginasweb.club/wp-content/uploads/2019/04/usuario.png'
    };
  }

  constructor() { }
}
