import { TestBed } from '@angular/core/testing';

import { FuelStationService } from './fuel-station.service';

describe('FuelStationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FuelStationService = TestBed.get(FuelStationService);
    expect(service).toBeTruthy();
  });
});
