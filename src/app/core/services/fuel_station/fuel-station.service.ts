import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subject, of } from 'rxjs';
import { Fuel_Station } from '../../model/fuel_station';
import { SortDirection } from '../../directives/directive';
import { tap, debounceTime, switchMap, delay, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


interface SearchResult {
  fuel_station: Fuel_Station[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(fuel_station: Fuel_Station[], column: string, direction: string): Fuel_Station[] {
  if (direction === '') {
    return fuel_station;
  } else {
    return [...fuel_station].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(fuel_station: Fuel_Station, term: string) {
  return fuel_station.name.toLowerCase().includes(term.toLowerCase()) ||
  fuel_station.location.toLowerCase().includes(term.toLowerCase())
}

@Injectable({
  providedIn: 'root'
})
export class FuelStationService {

  public _countries$ = new BehaviorSubject<Fuel_Station[]>([]);
  public _loading$ = new BehaviorSubject<boolean>(true);
  public _search$ = new Subject<void>();
  public _total$ = new BehaviorSubject<number>(0);
  public STATION:any = [];
  public delay = 0;
  public _state: State = {
    page: 1,
    pageSize: 6,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  }
  SERVER_URL: string = 'http://localhost:6008/fuelStation';

  constructor(private http: HttpClient) {this._search$.pipe(
        
    tap(() => this._loading$.next(true)),
    debounceTime(this.delay),
    switchMap(() => this._search()),
    delay(this.delay),
    tap(() => this._loading$.next(false))
  ).subscribe(result => {
    this._countries$.next(result.fuel_station);
    this._total$.next(result.total);
  });
  this._search$.next();
  }
  get countries$() {  return this._countries$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: string) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }


  getStation(): Observable<any> {
    return this.http.get(this.SERVER_URL).pipe(map((response:Response) =>{
      this.STATION= response;
      this._search$.next();
       console.log("Hello There", response);
      return this.STATION;
      
    }))
  }

  _search(): Observable<SearchResult> {
    
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let fuel_station = sort(this.STATION, sortColumn, sortDirection);
    
    // 2. filter
    fuel_station = fuel_station.filter(support => matches(support, searchTerm));
    const total = fuel_station.length;
    // 3. paginate
    fuel_station = fuel_station.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({fuel_station, total});
  }
   createStation(id, liter_price, name, location): Observable<any> {
     const data = {
       id, 
       liter_price: liter_price,
       name: name,
       location: location
     }
     return this.http.post(this.SERVER_URL, data).pipe(map((response:Response) => {
       return response;
     }))
   }
   updateStation(id, liter_price, name, location): Observable<any> {
     const data = {
      liter_price: liter_price,
       name: name,
       location: location
     }
     return this.http.put(this.SERVER_URL+('/')+id, data ).pipe(map((response:Response) => {
       return response;
     }))
   }
   deleteStation(id): Observable<any> {
     return this.http.delete(this.SERVER_URL+('/')+id).pipe(map((response: Response) => {
       return response;
     }))
   }

}