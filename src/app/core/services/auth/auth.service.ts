import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(   
     private http: HttpClient,
    private router: Router,
    private toastr: ToastrService) { }

    auth(user_name, password): Observable<any> {
      const data = {
        user_name: user_name,
        password: password
      }
  
      return this.http.post(environment.microservices.login.auth, data).pipe(map((response: Response) => {
        
        var respuesta  = response
        console.log(respuesta.toString())
        if(respuesta.toString() == "true"){
        this.router.navigateByUrl('/home');
      } else{
        this.toastr.success('El usuario no existe', 'Usuario no encontrado');
        console.log(data)
        console.log('Ok')
      }
    }))
  }
}


