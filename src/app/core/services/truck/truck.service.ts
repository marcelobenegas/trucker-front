import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subject, of } from 'rxjs';
import { Truck } from '../../model/truck';
import { SortDirection } from '../../directives/directive';
import { map, tap, debounceTime, switchMap, delay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { DecimalPipe } from '@angular/common';

interface SearchResult {
  truck: Truck[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(truck: Truck[], column: string, direction: string): Truck[] {
  if (direction === '') {
    return truck;
  } else {
    return [...truck].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(truck: Truck, term: string) {
  return truck.plate.toLowerCase().includes(term.toLowerCase()) ||
  truck.brand.toLowerCase().includes(term.toLowerCase()) ||
  truck.model.toLowerCase().includes(term.toLowerCase())
}

@Injectable({
  providedIn: 'root'
})
export class TruckService {
  public _countries$ = new BehaviorSubject<Truck[]>([]);
  public _loading$ = new BehaviorSubject<boolean>(true);
  public _search$ = new Subject<void>();
  public _total$ = new BehaviorSubject<number>(0);
  public TRUCK:any = [];
  public delay = 0;
  public _state: State = {
    page: 1,
    pageSize: 6,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  }

  SERVER_URL: string = 'http://localhost:6007/truck';

  constructor(private http: HttpClient, private pipe: DecimalPipe) {this._search$.pipe(
        
    tap(() => this._loading$.next(true)),
    debounceTime(this.delay),
    switchMap(() => this._search()),
    delay(this.delay),
    tap(() => this._loading$.next(false))
  ).subscribe(result => {
    this._countries$.next(result.truck);
    this._total$.next(result.total);
  });
  this._search$.next();

}

   get countries$() {  return this._countries$.asObservable(); }
   get total$() { return this._total$.asObservable(); }
   get loading$() { return this._loading$.asObservable(); }
   get page() { return this._state.page; }
   get pageSize() { return this._state.pageSize; }
   get searchTerm() { return this._state.searchTerm; }
 
   set page(page: number) { this._set({page}); }
   set pageSize(pageSize: number) { this._set({pageSize}); }
   set searchTerm(searchTerm: string) { this._set({searchTerm}); }
   set sortColumn(sortColumn: string) { this._set({sortColumn}); }
   set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

   private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
   }

   _search(): Observable<SearchResult> {
    
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let truck = sort(this.TRUCK, sortColumn, sortDirection);
    
    // 2. filter
    truck = truck.filter(support => matches(support, searchTerm));
    const total = truck.length;
    // 3. paginate
    truck = truck.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({truck, total});
  }

  getTruck(): Observable<any> {
    return this.http.get(this.SERVER_URL).pipe(map((response:Response) =>{
      this.TRUCK= response;
      this._search$.next();
       console.log("Hello There", response);
      return this.TRUCK;
      
    }))
  }
  createTruck(id, plate, brand, model): Observable<any> {
    const data = {
      id, 
      plate: plate,
      brand: brand,
      model: model
    }
    return this.http.post(this.SERVER_URL, data).pipe(map((response:Response) => {
      return response;
    }))
  }
  updateTruck(id, plate, brand, model): Observable<any> {
    const data = {
      id: id,
      plate: plate,
      brand: brand,
      model: model
    }
    return this.http.put(this.SERVER_URL+('/')+id, data ).pipe(map((response:Response) => {
      return response;
    }))
  }
  deleteTruck(id): Observable<any> {
    return this.http.delete(this.SERVER_URL+('/')+id).pipe(map((response: Response) => {
      return response;
    }))
  }
}

