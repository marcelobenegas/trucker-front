import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError, retry, tap, debounceTime, switchMap, delay, filter } from 'rxjs/operators';
import { Observable, Subject, BehaviorSubject, of } from 'rxjs';
import { Driver } from '../../model/driver';
import { DecimalPipe } from '@angular/common';
import { SortDirection } from '../../directives/directive';


interface SearchResult {
  driver: Driver[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(driver: Driver[], column: string, direction: string): Driver[] {
  if (direction === '') {
    return driver;
  } else {
    return [...driver].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(driver: Driver, term: string) {
  return driver.name.toLowerCase().includes(term.toLowerCase()) || 
  driver.run.toLowerCase().includes(term.toLowerCase()) ||
  driver.lastname.toLowerCase().includes(term.toLowerCase())
}

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  public _countries$ = new BehaviorSubject<Driver[]>([]);
  public _loading$ = new BehaviorSubject<boolean>(true);
  public _search$ = new Subject<void>();
  public _total$ = new BehaviorSubject<number>(0);
  public DRIVER:any = [];
  public delay = 0;
  public _state: State = {
    page: 1,
    pageSize: 6,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  }
  SERVER_URL: string = 'http://localhost:6009/driver';


  constructor(private http: HttpClient, private pipe: DecimalPipe) {
    this._search$.pipe(
        
      tap(() => this._loading$.next(true)),
      debounceTime(this.delay),
      switchMap(() => this._search()),
      delay(this.delay),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._countries$.next(result.driver);
      this._total$.next(result.total);
    });
    this._search$.next();

   }
   get countries$() {  return this._countries$.asObservable(); }
   get total$() { return this._total$.asObservable(); }
   get loading$() { return this._loading$.asObservable(); }
   get page() { return this._state.page; }
   get pageSize() { return this._state.pageSize; }
   get searchTerm() { return this._state.searchTerm; }
 
   set page(page: number) { this._set({page}); }
   set pageSize(pageSize: number) { this._set({pageSize}); }
   set searchTerm(searchTerm: string) { this._set({searchTerm}); }
   set sortColumn(sortColumn: string) { this._set({sortColumn}); }
   set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

   private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  _search(): Observable<SearchResult> {
    
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let driver = sort(this.DRIVER, sortColumn, sortDirection);
    
    // 2. filter
    driver = driver.filter(support => matches(support, searchTerm));
    const total = driver.length;
    // 3. paginate
    driver = driver.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({driver, total});
  }

  getDrivers(): Observable<any> {
    return this.http.get(this.SERVER_URL).pipe(map((response:Response) =>{
      this.DRIVER= response;
      this._search$.next();
      // console.log("Aqui estoy", response);
      return this.DRIVER;
      
    }))
  }
  
  // getDriver(): Observable<any> {

  //   return this.http.get('http://localhost:10355/driver/').pipe(map((response:Response) => {
  //     this.DRIVER = response['drivers'];
  //     console.log('actualizo')
  //     this._search$.next();
  //     return this.DRIVER;
  //   }))
  // }

  
  createDriver(id, run, name, last_name): Observable<any> {
    const data = {
      id, 
      run: run,
      name: name,
      last_name: last_name
    }
    return this.http.post(this.SERVER_URL, data).pipe(map((response:Response) => {
      return response;
    }))
  }

  updateDriver(id,run, name, last_name): Observable<any> {
    const data = {
      run: run,
      name: name,
      last_name: last_name
    }
    return this.http.put(this.SERVER_URL+('/')+id, data ).pipe(map((response:Response) => {
      return response;
    }))
  }

  deleteDriver(id): Observable<any> {

    return this.http.delete(this.SERVER_URL+('/')+id).pipe(map((response: Response) => {
      return response;
    }))
  }
}
